﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// This tells us we are using the UI
using UnityEngine.SceneManagement; // <-----------------------------------------------------------
// This is to reload the scene once you win!


            /* A NOTE ABOUT MY CODE & COMMENTS:
             * 
             * I use comments to type out my notes on code
             *   in class, so this (and all other code for
             *   at least this project) will have a lot of
             *   comments. For stuff I am going to add for
             *   this specific project, I will try to make
             *   it obvious. 
             *   (I put arrows pointing to the new stuff.)
             *   Thanks, Sorry!!       - C.             */

public class PlayerController : MonoBehaviour
{

    public float speed;

    public int Being_Great;

    private int count;
    // The number of pick ups we have

    // public GameObject wallToDestroy;
    // The above is for destroying the wall
    // (shown in class)

    private float fly = 0.0f; // <------------------------------------------------------------------
    // NEW! - This is my flying speed, for the win screen
    //        but also maybe in case I want to add flying?

    bool braking = false;

    public Text countText;
    // hold a ref to UI text component
    public Text winText;

    Rigidbody rb;
    // these are two variable our script can use
    // rigid body for our player (currently empty/nothing)


    // Start is called before the first frame update
    // Render here!!
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // THIS fills our ref. to our Rigidbody at top.
        // searches for game objects for components in our list
        // this.GetComponent<>

        count = 0;
        // sets the count

        winText.text = "";

        SetCountText ();
        // displays the count




    }

    // Update is called once per frame
    // Called every rendering frame.
    void Update()
    {
        // - GetKey is True all times the key is down
        // - GetKeyDown is only true when it is pressed
        // - GetKeyUp is only true when released.
        //
        //if(Input.GetKeyDown(KeyCode.Space))
        //{
        //    Instantiate(bulletPrefab, this.transform.position, bulletPrefab.transform.rotation);
        //                  Our Bullet,  the position of player, don't worry about it!
        //}
        // I've commented out all of this, but this is a cool example of
        //      how to create a bullet in the game.

        braking = Input.GetKey(KeyCode.Space);
        // not physics-y so we put it in update, but use later in FixedUpdate
        MisterResetti();
    }

    // Physical rendering, faster than the above Update.
    // Spelling matters! Capitals matter!
    private void FixedUpdate()
    {
        // Player Movement!
        // Not how they do it -> Vector2 Input =.....
        float moveHorizontal = Input.GetAxis("Horizontal");
            // returns input from player on horizontal axis
            // works with controllers and keyboard etc.
            //-1, 0, 1
        float moveVertical = Input.GetAxis("Vertical");
            //same but up & down
            // now construct Vector3 from 2 floats.
        Vector3 movement = new Vector3(moveHorizontal, fly, moveVertical);
        // zero on the y (0.0f) because we're not flying
        rb.AddForce(movement * speed);
        //player force is mvmnt times speed!
        //Because we put public float speed; at the top
        // we can now edit the speed in the inspector
        // window of Unity. HEY! THE BALL MOVES!

        if (braking) // <--------------------------------------------------------------------------------------
        {
            // This is for braking, which we did in class but I also feel like it helps
            //      manipulate the enemies in my current build of the game.

            float brakeAmount = .85f;
            rb.velocity = new Vector3(rb.velocity.x * brakeAmount, rb.velocity.y, rb.velocity.z * brakeAmount);
            // Our speed =            x velocity                   y velocity (0) z velocity
        }
    }
    // The next line is for collision, obvi.
    // Use function OnTriggerEnter
    // The collider is "other", the colliders we touch.
    private void OnTriggerEnter(Collider other)
    {
        // Destroy(other.gameObject);
        // 
        // The above code removes the obj & it's children from the game.
        // but we just want to DEACTIVATE this time around.
        // tag allows us to id. a game obj by comparing tag value to a string
        // set active - how we (de)activate an obj thru code
        // comparetag - compare tag of any obj to a String value.
        // You could use ->     if (gameObject.tag == "Some String Value")...
        // OR,
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive (false);
            // Deactivate the cubes

            count = count + 1;
            // Increase Score            

            SetCountText();


            // BELOW: If you get 3 cubes, a wall is destroyed!
            //        Comment this out before final build!
            //if (count == 3)
            //{
            //    Destroy(wallToDestroy);
            //}
        }
        // a Kinematic RigidBody will not react to physics and can be animated
        //                       & moved by their transform.
    }

    void SetCountText ()
    {
        countText.text = "Ghosts left: " + (13 - count).ToString(); // <---------------------
        // THIS FUNCTION UPDATES THE SCORE STRING.
        if (count >= 13)
        {
            winText.text = "Got 'em!";
            while (fly <= 1.5) // <----------------------------------------------------------
            {
                fly = fly + 0.01f;
                // The player drifts off into space when they win!
            }
            
        }
    }

    void MisterResetti () // <---------------------------------------------------------------
    {
        /* This code is to keep you from
         *      floating off into space
         *      forever. This resets the
         *      game once you have drifted
         *      far enough from home.  */

        if (rb.position.y >= 30)
        {
            SceneManager.LoadScene("PlayArea");
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
        // Currently called every time there is collision with anything
        // (walls, floor, a bumper, etc.)
        //if (collision.other.gameObject.CompareTag("Bumper"))
        //{
        //    Vector3 velWithoutZ = new Vector3(collision.relativeVelocity.x, 0f, collision.relativeVelocity.z);
        //    //  new Vector3 declared =        what it was hit at            0      what it was hit at
        //    rb.velocity += velWithoutZ * 2f;
        //    // our velocity that times two.
        //}
    //}

}

//UH OH!!


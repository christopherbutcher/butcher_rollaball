﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    Rigidbody npc; // New

    public float speed = 0.75f; // Created to adjust speed.
    Vector3 direction;

    public GameObject player;

    
    // Update is called once per frame
    private void Start()
    {
        // This whole start function is new.
        npc = GetComponent<Rigidbody>();

        direction = Vector3.down;

        transform.position = new Vector3(Random.Range(-9.4f, 9.4f), Random.Range(-1f, 1f), Random.Range(-9.4f, 9.4f));
        /* Here I created a new random starting position for the ghosts,
         *      each one starting somewhere in my above or below ground
         *      range and within the area between the four walls.    */
    }
    void Update()
    {
        // transform.Rotate (new Vector3 (15,0, 45) * Time.deltaTime);
        transform.Translate(direction * speed * Time.deltaTime);

        if(transform.position.y >= 1)
        {
            direction = Vector3.down * 0.25f;
            /* These two if statements represent the movement of the
             *       ghosts, moving up and down through the floor.
             *       I got this idea when my original plan of having
             *       the cubes run away from the player made my cubes
             *       instead rotate either out into space or into the
             *       ground.                                       */
        }

        if(transform.position.y <= -1)
        {
            transform.position = new Vector3(Random.Range(-9.4f, 9.4f), -1.0f, Random.Range(-9.4f, 9.4f));
            // The above line of code moves the ghost once it is out of view underground.
            direction = Vector3.up; // Going up!
        }
    }
} 
// - C.

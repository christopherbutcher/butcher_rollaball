﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// save a vector that is the camera pos minus the player pos
// when the ball moves around, add ball mvmnt back to that number
//      and then the camera will follow the ball! I think!



public class CameraController : MonoBehaviour
{
    // first we need a ref. to our ball
    public GameObject player;
    // drag & drop the Player from Hierarchy to Player in Inspector of Camera.
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
        
    }

    // Update is called once per frame
    // rarest of the 3, called last after everything else is done.
    // to keep the camera from jittering.
    
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
        // if the camera moved over here, the camera moves that distance.
    }
}
